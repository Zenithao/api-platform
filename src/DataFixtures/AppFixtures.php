<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Book;
use App\Entity\Review;
use DateTimeImmutable;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        $book = (new Book)
        ->setIsbn('9784063527650')
        ->setTitle($faker->sentence(4))
        ->setDescription($faker->paragraph(2))
        ->setAuthor($faker->name())
        ->setPublicationDate(new DateTimeImmutable());

        $manager->persist($book);

        for ($i = 0; $i <= 98; $i++) {
            $book = (new Book)
                ->setIsbn($faker->isbn13())
                ->setTitle($faker->sentence(4))
                ->setDescription($faker->paragraph(2))
                ->setAuthor($faker->name())
                ->setPublicationDate(new DateTimeImmutable());

                for ($j = 0; $j < 2; $j++) {
                    $review = (new Review)
                        ->setRating($faker->numberBetween(0, 5))
                        ->setBody($faker->paragraph(2))
                        ->setPublicationDate(new DateTimeImmutable())
                        ->setBook($book);
        
                        $manager->persist($review);
                }

                $manager->persist($book);
        }

        $manager->flush();
    }
}
